import './App.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import Home from "./pages/Home"
import Login from "./pages/Login"
import Register from "./pages/Register"
import Navbar from './components';
import Users from './pages/Users';
import Posts from './pages/Posts';


import { protectedRoute } from './util/protectedRoute';
import { isLoggedIn } from './util/protectedRoute';


const router = createBrowserRouter([
  {
    path: "/Home",
    element: <Home />,
    loader: protectedRoute
  },
  {
    path: "/",
    element: <Login />,
    loader: isLoggedIn
  },
  {
    path: "/Register",
    element: <Register />,
    loader: protectedRoute
  },
  {
    path: "/Users",
    element: <Users />,
    loader: protectedRoute
  },
  {
    path: "/Posts",
    element: <Posts />,
    loader: protectedRoute
  },
  {
    path: "*",
    element: <h1>404 Not Found</h1>,
  },
]);

function App() {
  

  return (
    <>
      <Navbar />
      <RouterProvider router={router} />
    </>
  );
}

export default App
