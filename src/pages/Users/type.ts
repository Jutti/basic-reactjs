export interface IProp {}

export interface IState {
    userList:IUser[];
}


export interface IUser {
    _id:number
    name:string
    username:string
    email:string
    role:string
    password: string
    
}