import * as React from 'react';
import { useEffect, useState } from "react";
import axios from "axios";
import { IProp, IState, IUser } from "./type";
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';



    const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
    }));


function Users(props: IProp) {
    const [userlist , setUserList] = useState<IState["userList"]>([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] =useState("");
    const [role, setRole] = useState("");
    const [editUserList, setEditUserList] = useState<IUser | null>(null);

    
    useEffect(() => {
        handleRequestUsers();
    }, []);


    const handleRequestUsers = async () => {
        try{
            const response = await axios.get(
                "http://localhost:5173/api/user"
            );
            setUserList(response.data);
        } catch (error) {
            alert('ERROR')
        }
    };

    const handleAddButton = async() => {
        try {
            const response = await axios.post(
                "http://localhost:5173/api/user",
                { name, email, password , role } 
            );
            handleRequestUsers();
            setName("");
            setEmail("");
            setPassword("");
            setRole("");
            // setShowDetails(false);
        } catch (error) {
            console.log('error', error)
        }
    }


    const handleEditOnChange= (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement> ) => {
        const { name, value } = event.target;
        if (editUserList) {
            setEditUserList({ ...editUserList, [name]: value });
        }
    }


    return (
        <div className="form"> 
            <h1>User Page</h1>
            <TextField id="outlined-basic" color="secondary" label="Name" variant="outlined" value={name} onChange={(event) => setName(event.target.value)} name="name"/><br/><br/>
            <TextField id="outlined-basic" color="secondary" label="Email" variant="outlined" value={email} onChange={(event) => setEmail(event.target.value)} name="email"/><br/><br/>
            <TextField id="outlined-basic" color="secondary" label="Passoword" variant="outlined" value={password} onChange={(event) => setPassword(event.target.value)} name="password"/><br/><br/>
            <FormControl sx={{ m: 1, minWidth: 80 }}>
                <InputLabel id="demo-simple-select-autowidth-label">Role</InputLabel>
                <Select
                    labelId="demo-simple-select-autowidth-label"
                    id="demo-simple-select-autowidth"
                    autoWidth
                    label="Age"
                    value={role} 
                    onChange={(event) => setRole(event.target.value)} 
                    name="role" 
                    
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value="Admin">Admin</MenuItem>
                    <MenuItem value="Lead">Lead</MenuItem>
                    <MenuItem value="Developer">Developer</MenuItem>
                </Select>
            
            </FormControl>
            <br/><br/><Button variant="contained" color="secondary" onClick={handleAddButton}>Create User</Button><br/><br/>
            <br/>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Name</StyledTableCell>
                            <StyledTableCell align="right">Email</StyledTableCell>
                            <StyledTableCell align="right">Password</StyledTableCell>
                            <StyledTableCell align="center">Role</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody> 
                    {userlist.map((user) => (
                            <StyledTableRow key={user._id}>
                                <StyledTableCell component="th" scope="row">
                                    {editUserList && editUserList._id === user._id ? <input name="name" type="text" value={editUserList.name} onChange={handleEditOnChange} /> : user.name}
                                </StyledTableCell>

                                <StyledTableCell align="right">
                                    {editUserList && editUserList._id === user._id ? <input name="email" type="text" value={editUserList.email} onChange={handleEditOnChange} /> : user.email}
                                </StyledTableCell>

                                <StyledTableCell align="right">
                                    {editUserList && editUserList._id === user._id ? <input name="password" type="text" value={editUserList.password} onChange={handleEditOnChange} /> : user.password}
                                </StyledTableCell>
                                <StyledTableCell align="right">
                                    {editUserList && editUserList._id === user._id ?
                                    <select name="role" value={editUserList.role} onChange={handleEditOnChange}>
                                        <option value="Admin">Admin</option>
                                        <option value="Lead">Lead</option>
                                        <option value="Developer">Developer</option>
                                    </select> :
                                    user.role}
                                </StyledTableCell>
                            </StyledTableRow>
                               
                            
                        ))
                    }
                    </TableBody>
                    
                </Table>
            </TableContainer>   
                
        </div>
    )
}



export default Users;