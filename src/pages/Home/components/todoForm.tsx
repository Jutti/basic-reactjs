import React from "react";
import "../style.css"
import { ITodoFormProp } from "../type";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';



function TodoForm(props: ITodoFormProp) {
    const { inputHandler , messageHandler , btnAddHandler , temp_input , temp_message } = props;

        return (
            <div>
                <TextField 
                    id="outlined-basic" 
                    color="secondary" 
                    label="Title" 
                    variant="outlined"
                    name="title"
                    type="text"
                    onChange={inputHandler}
                    value={temp_input}
                />
                <br/>
                <br/>
                <TextField 
                    id="outlined-basic" 
                    color="secondary" 
                    label="Message" 
                    variant="outlined"
                    name="message"
                    onChange={messageHandler}
                    value={temp_message}
                /><br/><br/>
                <Button variant="contained" color="success" onClick={btnAddHandler}> Add 
                </Button><br/>
            </div>
        );
}


export default TodoForm;