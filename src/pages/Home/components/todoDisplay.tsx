import React, { useState } from "react";
import "../style.css"
import { ITodoDisplayProp } from "../type";
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';





const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


function TodoDisplay (props: ITodoDisplayProp){
    const { inputlist , showTable, actionDelete , editItem , actionEdit, handleEditOnChange , handleUpdateButton ,handleCancelButton } = props;

    return (
    <>
    {!showTable ? (
        <></>
        ) : (
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Title</StyledTableCell>
                            <StyledTableCell align="right">Message</StyledTableCell>
                            <StyledTableCell align="center">Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody> 
                    {inputlist.map((items,index) => {
                        if (items.id ===editItem?.id && items.id) {
                        return (
                            <StyledTableRow key={items.id + index}>
                                <StyledTableCell component="th" scope="row">
                                <TextField id="outlined-basic" label="Outlined" variant="outlined" name="editInput" value={editItem.input} onChange={handleEditOnChange} />
                                </StyledTableCell>

                                <StyledTableCell component="th" scope="row">
                                <TextField id="outlined-basic" label="Outlined" variant="outlined" name="editMessage" value={editItem.message} onChange={ handleEditOnChange} />
                                </StyledTableCell>

                                <StyledTableCell component="th" scope="row">
                                    <Button variant="contained" color="success" onClick={handleUpdateButton}> Update </Button>
                                    <Button variant="contained" color="success" onClick={handleCancelButton}> Cancel </Button>
                                </StyledTableCell>
                            </StyledTableRow>
                        );
                    } else {
                        return (
                            <StyledTableRow key={items.id + index}>
                                <StyledTableCell component="th" scope="row">
                                    <span>{items.input}</span>
                                    </StyledTableCell>
                                <StyledTableCell component="th" scope="row">{items.message}</StyledTableCell>
                                <StyledTableCell component="th" scope="row">
                                    <Button 
                                        variant="contained" 
                                        color="success"
                                        onClick={() => actionEdit(items)}
                                    >
                                        Edit
                                    </Button>
                                    <Button 
                                        variant="contained" 
                                        color="success"
                                        onClick={() => actionDelete(items.id)}
                                        
                                    >
                                        Delete
                                    </Button>
                                    </StyledTableCell>
                            </StyledTableRow>
                        );
                    }
                    })}
                </TableBody>
            </Table>
            </TableContainer>
        )}   
    </>
    );
}


export default TodoDisplay;