    import React, { useState } from "react";
    import { IProp, ITodo} from "./type";
    import './style.css'
    import TodoForm from "./components/todoForm";
    import TodoDisplay from "./components/todoDisplay";
    import { useNavigate } from "react-router-dom";
    import Button from '@mui/material/Button';

    const Home = (props: IProp) => {
        const navigate = useNavigate();
        const [temp_input, setTempInput] = useState("");
        const [temp_message, setTempMessage] = useState("");
        const [input_list, setInputList] = useState<ITodo[]>([]);
        const [showTable, setShowTable] = useState(false);
        const [editItem, setEditItem] = useState<ITodo>({id: "", input: "", message: ""});    


        const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) =>{
            setTempInput(event.target.value);
        };

        const messageHandler = (event:React.ChangeEvent<HTMLInputElement>) =>{
            setTempMessage(event.target.value)
        };

        const btnAddHandler = () =>{
            const newInputList = [
                ...input_list, 
                {
                    id: `${new Date().getTime() / 1000}`,
                    input: temp_input, 
                    message: temp_message, 
                }
            ];
                setInputList(newInputList), 
                setTempInput(""), 
                setTempMessage(""); 
            };
            

        const handleCheckShowTable = ( event:React.ChangeEvent<HTMLInputElement>) => {
            setShowTable(event.target.checked);
        }

        const handleActionDelete = (id:string) => {
            const newInputList = input_list.filter((item) => item.id !==id);
            setInputList(newInputList);
        }

        const handleEditButton = (item:ITodo) => {
            setEditItem(item);
        };

        const handleEditOnChange= (event: React.ChangeEvent<HTMLInputElement>) => {
            const { name, value }  = event.target;
            if(name === 'editInput'){
                setEditItem({...editItem, input: value});
            } else if(name === 'editMessage'){
                setEditItem({...editItem, message: value});
            }
        }

        const handleUpdateButton = () => {
            setInputList(input_list.map(item => (item.id === editItem.id ? { ...editItem} : item)));
            setEditItem({id:'' , input: '' , message: ''});

        };


        const handleCancelButton = () => {
            setEditItem({
                id: '',
                input: '',
                message: '',
            });
        };

        const handleButtonLogout = () => {
            localStorage.removeItem('Token')
            navigate('/')
        }

        
        return (
            <div className="form">
                <br/>
                <h1> Home Page</h1>
                <div className="form">
                    <TodoForm
                        btnAddHandler={btnAddHandler}
                        inputHandler={inputHandler}
                        messageHandler={messageHandler}
                        temp_input={temp_input}
                        temp_message={temp_message}
                    />
                </div><br/>
                <input type="checkbox" onChange={handleCheckShowTable} /> 
                <span>Show Table</span>
                
                <TodoDisplay
                    showTable={showTable} 
                    inputlist={input_list}
                    actionDelete={handleActionDelete}
                    actionEdit={handleEditButton}
                    editItem={editItem}
                    handleEditOnChange={handleEditOnChange}
                    handleUpdateButton={handleUpdateButton}
                    handleCancelButton={handleCancelButton} />
                    <br/><br/><br/><br/><br/>
                    <Button variant="contained" color="error" onClick={handleButtonLogout}>Logout</Button>
            </div>
            
                
        );
    };

    export default Home;