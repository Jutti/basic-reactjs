import { IProp } from "./type";
import { Navigate, useNavigate } from "react-router-dom";
import React, { useState } from "react";
import { accounts } from "../../util/authenAccounts";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';


function Login(props: IProp){
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUserName] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const handleInputUser = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event?.target;
        setUserName(value);
    }

    const handleInputPass = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event?.target;
        setPassword(value);
    }

    const navigate = useNavigate();

    const handleButtonLogin = () => {
        const token = {
            username,
            password,
        };

        const authenticated = accounts
            .map((item) => JSON.stringify(item))
            .includes(JSON.stringify(token));
        if(authenticated){
            localStorage.setItem("Token", JSON.stringify(token));
            navigate("/Home");
        } else {
            alert("Invalid Account")
        }
    };

    const handleButtonCreate = () => {
        navigate('/Register');
    }

    return (
        <div className="form">
            <h1>Login Page</h1>
            <TextField id="outlined-basic" color="secondary" label="Username" variant="outlined" onChange={handleInputUser} value={username} /><br/><br/>
            <TextField id="outlined-basic" color="secondary" label="Password" variant="outlined" type={showPass ? "text" : "password"} onChange={handleInputPass} value={password} /><br/><br/>
            <input type="checkbox" checked={showPass} onChange={() => setShowPass(prev => !prev)} /> <span>See Password</span>
            <br/><br/>
            <Button variant="contained" color="success" onClick={handleButtonLogin}>Login</Button>
            <br/><br/><br/>
            <h3>No account? Click Register to login</h3>
            <Button variant="contained" color="success" onClick={handleButtonCreate}>Register</Button>

        </div>
        
    )
}

export default Login