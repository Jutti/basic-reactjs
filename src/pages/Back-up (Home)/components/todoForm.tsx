import React from "react";
import "../style.css"
import { ITodoFormProp } from "../type";



function TodoForm(props: ITodoFormProp) {
    const { inputHandler , messageHandler , btnAddHandler , temp_input , temp_message } = props;

        return (
            <div>
                <input
                    name="title"
                    type="text"
                    onChange={inputHandler}
                    value={temp_input}
                />
                <br/>
                <br/>
                <textarea
                    name="message"
                    onChange={messageHandler}
                    value={temp_message}
                ></textarea><br/><br/>
                <button className="btn" onClick={btnAddHandler}> Add 
                </button>
            </div>
        );
}


export default TodoForm;