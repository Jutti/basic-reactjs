import React from "react";
import { IProp, IState, ITodo} from "./type";
import './style.css'
import TodoForm from "./components/todoForm";
import TodoDisplay from "./components/todoDisplay";


class Home extends React.Component<IProp , IState>{
    constructor(props: IProp) {
        super(props);
        this.state ={
            temp_input:"",
            temp_message:"",
            input_list: [],
            showTable: false,
            editItem: {
                id: '',
                input: '',
                message: '',
            },
        };
    }
    

    inputHandler = (event: any) =>{
        const { value } = event.target;
        this.setState({temp_input:value})
    }

    messageHandler = (event:any) =>{
        const { value } = event.target;
        this.setState({temp_message:value})
    }

    btnAddHandler = () =>{
        const { temp_input, temp_message, input_list } = this.state;
        const newInputList = [
            ...input_list, 
            {
                id: `${new Date().getTime() / 1000}`,
                input: temp_input, 
                message: temp_message, 
            }
        ];
        this.setState({
            input_list: newInputList, 
            temp_input: "", 
            temp_message: "" 
        });
        
    };

    handleCheckShowTable = ( event:any) => {
        const { checked } = event.target;
        this.setState ({ showTable:checked});
    }

    handleActionDelete = (id:string) => {
        const { input_list } = this.state;
        const newInputList = input_list.filter((item) => item.id !==id);
        this.setState({ input_list: newInputList });
    }

    handleEditButton = (item:ITodo) => {
        this.setState({editItem:{id: item.id, message: item.message, input: item.input}});
    };

    handleEditOnChange= (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name,value} = event.target;
        if(name === "editInput") {
            this.setState({editItem: {...this.state.editItem, input: value}})
        }
        else if(name === "editMessage"){
            this.setState({editItem: {...this.state.editItem, message: value}})
        }
    }

    handleUpdateButton = () => {
        const { editItem, input_list } = this.state;

        const newList = input_list.map(items => {
            if(items.id === editItem.id){
                return editItem
            }
            return items;
        })
        this.setState({input_list: newList, editItem: {id:'' , input: '' , message: ''}})
    }

    handleCancelButton = () => {
        this.setState({editItem: {
            id: '',
            input: '',
            message: '',
        },})
    }

    render(){
        return (
            <div>
                <h1> Home Page</h1>
                <div className="basic-form-container">
                    <TodoForm
                        btnAddHandler={this.btnAddHandler}
                        inputHandler={this.inputHandler}
                        messageHandler={this.messageHandler}
                        temp_input={this.state.temp_input}
                        temp_message={this.state.temp_message}
                    />
                </div>
                <input type="checkbox" onChange={this.handleCheckShowTable} /> 
                <span>Show Table</span>
                <TodoDisplay
                showTable={this.state.showTable} 
                inputlist={this.state.input_list}
                actionDelete={this.handleActionDelete}
                actionEdit={this.handleEditButton}
                editItem={this.state.editItem}
                handleEditOnChange={this.handleEditOnChange}
                handleUpdateButton={this.handleUpdateButton}
                handleCancelButton={this.handleCancelButton} />
            </div>
            
        )
    }
}

export default Home;