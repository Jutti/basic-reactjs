import { useState, useEffect } from "react";
import axios from "axios";
import { IProp, IState, IPost } from "./type";
import { BASE_URL , postEndPoint } from "../../util/endpoint";
import './style.css'
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


function Posts(props: IProp){
    const [postList, setPostList] = useState<IState['postList']>([])
    const [title, setTitle] = useState("");
    const [message, setMessage] = useState("");
    const [showDetails, setShowDetails] = useState(false);
    const [editPostList, setEditPostList] = useState<IPost | null>(null);


    useEffect(() => {
        handleRequestPost();
    }, []);

    const handleRequestPost = async() => {
        try {
            const response = await axios.get(
               `${BASE_URL}/${postEndPoint}`
            );
            setPostList(response.data);
            setShowDetails(false);
        } catch (error){
            alert('ERROR')
        }
    }

    const handleAddButton = async() => {
        try {
            const response = await axios.post(
                `${BASE_URL}/${postEndPoint}`,
                { title, message } 
            );
            handleRequestPost();
            setTitle("");
            setMessage("");
            // setShowDetails(false);
        } catch (error) {
            console.log('error', error)
        }
    }

    
    const handleDeleteButton = async(id: number) => {
        try {
            await axios.delete(
                `http://localhost:5173/api/post/${id}`
            );
            setPostList(prevPostlist => prevPostlist.filter(item => item._id !== id));
        } catch (error) {
            console.log('error', error) 
        }
    }

    const handleEditOnChange= (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (editPostList) {
            setEditPostList({ ...editPostList, [name]: value });
        }
    }

    const handleCancelButton = () => {
        setEditPostList(null);
    };


    const handleUpdateButton = async() => {
        if (editPostList) {
            try {
                await axios.put(`http://localhost:5173/api/post/${editPostList._id}`, { title: editPostList.title, message: editPostList.message });
                handleRequestPost();
                setEditPostList(null);
                alert('Updated Successfully!')
            } catch (error) {
                console.log('error', error);
            }
        }
    };


    const handleEditButton = (items: IPost) => {
        setEditPostList(items);
    }

    return (
        <div className="form"> 
            <h1>POST</h1>
            <TextField id="outlined-basic" color="secondary" label="Title" variant="outlined" value={title} onChange={(event) => setTitle(event.target.value)} name="editTitle"/><br/><br/>
            <TextField id="outlined-basic" color="secondary" label="Body" variant="outlined" value={message} onChange={(event) => setMessage(event.target.value)} name="editMessage"/><br/><br/>
            <Button variant="contained" color="success" onClick={handleAddButton}>Add</Button><br/><br/>
            <Button variant="contained" color="secondary" onClick={() => setShowDetails(!showDetails)}> {showDetails ? 'Hide Details' : 'Show Details'} </Button><br/><br/>
            <br/>
            {showDetails && (
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Title</StyledTableCell>
                            <StyledTableCell align="right">Message</StyledTableCell>
                            <StyledTableCell align="right">Status</StyledTableCell>
                            <StyledTableCell align="center">Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody> 
                        {postList.map((post) => (
                            <StyledTableRow key={post._id}>
                                <StyledTableCell component="th" scope="row">{editPostList && editPostList._id === post._id ?
                                     <TextField id="outlined-basic" label="Outlined" variant="outlined" name="title" value={editPostList.title} onChange={handleEditOnChange} /> : post.title} 
                                </StyledTableCell>

                                <StyledTableCell align="right">{editPostList && editPostList._id === post._id ? 
                                    <TextField id="outlined-basic" label="Outlined" variant="outlined" name="message" value={editPostList.message} onChange={handleEditOnChange} /> : post.message}
                                </StyledTableCell>

                                <StyledTableCell align="right" 
                                    className={post.status === 'Inactive' ? 'red' : 'green'}>
                                        {post.status}
                                </StyledTableCell>

                                <StyledTableCell align="center">
                                <td>
                                    {editPostList && editPostList._id === post._id ? (
                                        <>
                                            <Button variant="contained" color="success" onClick={handleUpdateButton}> Update </Button>
                                            <Button variant="contained" color="error" onClick={handleCancelButton}> Cancel </Button>
                                        </>
                                    ) : (
                                        <>
                                            <Button variant="contained" color="success" onClick={() => handleEditButton(post)}> Edit </Button>
                                            <Button variant="contained" color="error" onClick={() => handleDeleteButton(post._id)}> Delete </Button>
                                        </>
                                    )}
                                </td>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>    
            </TableContainer>   
                )}
        </div>
    );
}
    
    
export default Posts;