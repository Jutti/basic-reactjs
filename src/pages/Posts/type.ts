export interface IProp {}

export interface IState {
    postList: IPost[]
}

export interface IPost{
    _id: number;
    userId: number;
    title: string;
    body: string;
    message: string
    status: string
    
}

