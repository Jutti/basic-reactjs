import { IProp } from "./type";
import { useNavigate } from "react-router-dom";

import React from "react";


function Register(props: IProp){
    const navigate = useNavigate();


    const handleRegisterButton = () => {
        navigate('/')
    }
    return (
        <div className="form">
            <h1>Registration Page</h1>
            <h2>Create username: <input type="text" id="uname" name="uname"/></h2>
            <h2>Create Password: <input type="password" id="pword" name="pword"/></h2>
            <br/>
            <br/>
            <button className="btn" onClick={handleRegisterButton}>Create</button>
        </div>
    )
}

export default Register