import React from "react";
import { IProp } from "./type";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Avatar from '@mui/material/Avatar';

export default function Navbar() {
    return (
        <div className="form">
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" >
                <Toolbar>
                    <a href="/" style={{ textDecoration: 'none', color: 'inherit', margin: 'auto' }}>
                        Home
                    </a>
                    <a href="/users" style={{ textDecoration: 'none', color: 'inherit', margin: 'auto' }}>
                        Users
                    </a>
                    <a href="/posts" style={{ textDecoration: 'none', color: 'inherit', margin: 'auto' }}>
                        Posts
                    </a>
                    <Avatar alt="Jutti" src="sioming.jpg" />
                </Toolbar>
            </AppBar>
        </Box>
        </div>
    );
}






// import React from "react";
// import './style.css'
// import { IProp } from "./type";



// function Navbar(props: IProp) {
//     return (
//         <nav className="nav-style">
//             <a href="/about">About</a>
//             <a href="/users">Users</a>
//             <a href="/posts">Posts</a>
//         </nav>
        
//     );
// }

// export default Navbar;